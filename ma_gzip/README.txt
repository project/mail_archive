Overview:
--------
The ma_gzip module is an add on for the mail archive module.  It provides
gzip compression and decompression of messages downloaded from mailing lists.
This module requires that you compile php with the "--with-zlib" option.


Features:
--------
 o gzip compresses messages with zlib
 o compression can be disabled, and the module will still provide decompression
 o can be configured to compress with any level from 1 (least/fastest) to 9 
   (most/slowest)
 o detects whether or not zlib was compiled into your php installation


Requires:
--------
 - Drupal 4.5
 - PHP configured with zlib support (http://www.gzip.org/zlib/)


Credits:
-------
 - Written by Jeremy Andrews <jeremy@kerneltrap.org>
