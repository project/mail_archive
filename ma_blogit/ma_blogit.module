<?php
// $Id:

/*
** ma_blogit.module:
** Adds "blog it" tab when viewing messages and threads with the mail archive
** module.
** Written by Jeremy Andrews <jeremy@kerneltrap.org>, January 2005.
*/


/***********************************************\
** this section provides all the Drupal hooks **
\***********************************************/
function ma_blogit_help($section) {
  $output = '';
  switch ($section) {
    case 'admin/modules#description':
      $output = t('Provides a "blog it" tab when viewing messages with the mail archive module.');
    case 'admin/settings/ma_blogit':
      $output = t('Customize the layout of messages that are turned into blogs.');
      break;
  }
  return $output;
}

function ma_blogit_menu($may_cache) {
  $items = array();
  if (!$may_cache) {
    $lid = arg(1);
    $op = arg(2);
    $mid = arg(3);
    $style = arg(4);
    if ($op == 'message') {
      $items[] = array('path' => "mailarchive/$lid/message/$mid/$style/blogit",
                       'title' => t('blog it'),
                       'callback' => 'ma_blogit_page',
                       'access' => user_access('edit own blog'),
                       'type' => MENU_LOCAL_TASK,
                       'weight' => 5);
    }
  }
  return $items;
}

function ma_blogit_settings() {
  $output = form_textarea(t('Message layout'), 'ma_blogit_layout', variable_get('ma_blogit_layout', _ma_blogit_layout()), 70, 10, t('Customize the text created when a mail archive messsage is turned into a blog with the "blog it" link.  The following variables can be used: %list (the name of the mailing list the message was posted to), %from (who the message was from), %to (who the message was to), %subject (the subject of the message), %date_small (when the message was received, small formating), %date_large (when the message was received, large formatting), %body (the body of the message), %link (link to the message or thread, becomes the word "message" or "thread" depending on what it is linking to).'));
  return $output;
}

function _ma_blogit_layout() {
  return t("On %date_small %from wrote on the %list mailing list a message titled '%subject':\n<blockquote><pre>\"%body\"</pre></blockquote>\nRead the full %link.");
}

function ma_blogit_page() {
  $op = $_POST['op'];
  $edit = $_POST['edit'];

  $lid = arg(1);
  $mid = arg(3);
  $style = arg(4);
  switch ($op) {
    case t('Preview'):
      $edit = node_validate($edit);
      print theme('page', node_preview($edit), t('Preview'));
      break;
    case t('Submit'):
      drupal_set_title(t('Submit'));
      print theme('page', node_submit($edit));
      break;
    default:
      $message = module_invoke('mail_archive', 'load_message', $lid, $mid);
      if ($message->mid) {
        $listname = module_invoke('mail_archive', 'get_subscription_name', $lid);
        $title = $message->subject_short ? $message->subject_short : t('(No subject)');
        if ($style == 'thread') {
          $body = strtr(variable_get('ma_blogit_layout', _ma_blogit_layout()), array('%from' => $message->mailfrom_short, '%to' => $message->mailto, '%subject' => $message->subject_short, '%date_small' => format_date($message->received, 'short'), '%date_long' => format_date($message->received, 'large'), '%list' => l($listname, "mailarchive/$lid/thread"), '%body' => module_invoke('mail_archive', 'retrieve_message', $message->path, "$message->mid" .'.1'), '%link' => l(t('thread'), "mailarchive/$message->lid/message/$message->mid/thread")));
        }
        else {
          $body = strtr(variable_get('ma_blogit_layout', _ma_blogit_layout()), array('%from' => $message->mailfrom_short, '%to' => $message->mailto, '%subject' => $message->subject_short, '%date_small' => format_date($message->received, 'short'), '%date_long' => format_date($message->received, 'large'), '%list' => l($listname, "mailarchive/$lid/thread"), '%body' => module_invoke('mail_archive', 'retrieve_message', $message->path, "$message->mid" .'.1'), '%link' => l(t('message'), "mailarchive/$message->lid/message/$message->mid/message")));
        }
        $_GET['edit']['title'] = $title;
        $_GET['edit']['body'] = $body;
        print theme('page', node_add('blog'));
      }
      break;
  }
}
