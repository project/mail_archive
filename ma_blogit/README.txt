Overview:
--------
The ma_blogit module is an addon for the mail archive module.  It provides
a simple "blog it" link from messages that when clicked will turn a message
into a blog entry.


Features:
--------
 o provides 'blog it' menu option
 o the resulting blog layout can be customized by the admin


Requires:
--------
 - Drupal 4.5
 - mail_archive module


Credits:
-------
 - Written by Jeremy Andrews <jeremy@kerneltrap.org>
