Overview:
--------
The mailarchive module allows you to subscribe to one or more mailing lists
which will then be stored in a threaded archive.  Much like groups.google.com
or marc.theaimsgroup.com, it is possible for users to navigate through the
mailing list archives.

CAUTION:  This is still a work in progress.  It is functional, and you may
enjoy testing the module and submitting fixes in the form of patches.  However,
it is not recommended that you use this module on a production system yet.
The smaller the todo list is, the closer this module is to being released.



Features:
--------
 o can subscribe to an unlimited number of mailing lists
 o provides an optional forum-like overview page for displaying a categorized
   list of all subscribed mailing lists and how many messages each has
 o subscriptions are "nodes", meaning they support taxonomy, comments, etc
 o messages and attachments are stored to the filesystem
 o offers flat and threaded message views
 o offers ability to browse by year, month, week and day
 o properly threads messages that arrive out of order
 o provides administrative interface to delete messages
 o provides a limited (to-be-expanded) api for use by addon modules


Addons:
------
 o ma_bzip2 addon module compresses messages with bzip2
 o ma_gzip addon module gzip compresses messages with zlib
 o ma_blogit addon module provides "blog it" link for messages


Todo:
----
(items in this list are features I plan to implement before releasing this
 module)
 o display flat (subject/author) in addition to expanded (full text) threads
 o display only n chars from each message when displaying thread
 o searching by subject, from, to, cc (anything already indexed)
 o addon: ability to create nodes from individual messages or entire threads
   (or arbitrary collection of messages)  (node can be any type:  forum, 
    story, etc)
 o addon: ability to tag messages and threads as "interesting" and display
   in block
 o addon: filter spam messages by recognizing spamassassin headers
 o addon: track which posts a given user has read
 o cleanup database, removing unused columns & indexes

Wishlist:
--------
(items in this list are features I think would be nice, but that I'm not
 currently working toward)
 o ability for users to post to subscribed mailing lists
 o provide RSS feeds of mailing lists (and searches, ie all messages by
   certain author, with certain subject, etc)
 o hook into existing Drupal search engine to search body's of messages
 o addon: save messages to database instead of filesystem


Requires:
--------
 - Drupal 4.5
 - PHP configured with imap support (http://www.php.net/imap)


Credits:
-------
 - Written by Jeremy Andrews <jeremy@kerneltrap.org>
