Overview:
--------
The ma_bzip2 module is an add on for the mail archive module.  It provides
bzip2 compression and decompression of messages downloaded from mailing lists.
This module requires that you compile php with the "--with-bz2" option.


Features:
--------
 o compresses messages with bzip2
 o compression can be disabled, and the module will still provide decompression
 o can be configured to compress with any level from 1 (least/fastest) to 9 
   (most/slowest)
 o decompression algorithm can be configured to have small memory footprint
 o detects whether or not bz2 was compiled into your php installation


Todo:
----
 o detect if compressed file is smaller, if not use original


Requires:
--------
 - Drupal 4.5
 - PHP configured with bz2 support (http://sources.redhat.com/bzip2/)


Credits:
-------
 - Written by Jeremy Andrews <jeremy@kerneltrap.org>
